# Cute app

Toy project to re-learn qt.


## Notes

This readme was snatched from libsplb2, and may contain typo.

This readme/setuping guide assumes that the user is running a **Debian** (version 11 has been tested and is working) distribution or a **Windows 10** (tested on v1909) OS.

This guides will explain how to build the lib with llvm/gnu and ninja. On Windows you may use MSVC but although it will work, I don't recommend it.

## Required system packages

You'll need a good build system configuration generator (cmake is the only solution used here, and you don't need more).

You'll need a good cross-platform build system (ninja is the only solution used here, and you don't need more).

You'll need a good compiler (gnuc/++ or clang/++).

### For Debian

```
$ sudo apt install cmake ninja-build build-essential clang lcov clang-tidy qtbase5-dev qtcreator
```

You may want to compile and install: https://github.com/include-what-you-use/include-what-you-use

### For Windows

You'll want exactly the same thing as Debian. You can find the tools using the following links:
```
https://cmake.org/download/
https://releases.llvm.org/ # you may need to go to the github release page to get the latest version (not required)
https://ninja-build.org/
```
On Windows when installing LLVM, make sure to check the "**Add LLVM to the system PATH for all users**" !!

On Windows ninja is not a package that is to be installed(like the debian pkg). You can put it wherever you want as long as it's in the PATH environment variable (it makes things easier). I recommend to put it in the `LLVM/bin` folder that you should have obtained after installing LLVM.

You may want to restart `explorer.exe` or reboot your machine to apply the PATH settings.

### Version constraints

```
gnuc/c++ compilers >= 9.3.0  is great, newer is better, MAY work with older version
clang             >= 11.0.1 is great, newer is better, MAY work with older version
cmake             >= 3.18.4 is great, newer is better, MAY work with older version
ninja             >= 1.10.2 is great, newer is better, MAY work with older version
```

## Cloning the repository :

1. Clone the repository
```bash
$ git clone git@gitlab.com:etiennemlb/simply_bad_2.git
```
2. Install & Update the required packages. Please refer to the `Build Setup` section.

## Contact

* Étienne Malaboeuf - eti.malaboeuf@gmail.com

## Details and acronyms

None

## Generating the doc

Only supported on Debian (though it's absolutely possible to generate the doc on Windows).

Install doxygen :

```
$ sudo apt install doxygen
```

### Version constraints

```
doxygen >= 1.9.1 is great, newer is better, MAY work with older version
```

### Generate the doc

Run this command when in the root of the project:
```
$ mkdir build && cd build
$ cmake -G "Ninja" ..
$ ninja doc
```

The generated doc start at "docs/doxygen/html/index.html" (in the build folder).

## Build

First make sure all the packages are installed (See `Requiered packages`).

What's nice is that with cmake, ninja and llvm we have a "fully" cross platform tool chain (no need to use your dear/hated msvc/make/name yours).

Under Windows and Debian only the shell changes, not the command :).
```
$ mkdir build && cd build                         # or Debug or Release, it doesn't matter, this is where the build files will be
$ cmake -G "Ninja" -DCMAKE_BUILD_TYPE=YOURTYPE .. # Check YOURTYPE below, this matter !! Default to Release
$ ninja                                           # Start building
```

```
YOURTYPE can be : [Release|MinSizeRel|RelWithDebInfo|Debug]
```

If you want to switch compiler, check these PATH env variables :

```
CXX # can be [clang++|g++]
CC  # can be [clang|g]
```

All the tools you require should be on the path, if it's not the case you may want to troubleshoot/check these PATH env variables :
```
-DCMAKE_MAKE_PROGRAM
-DCMAKE_C_COMPILER
-DCMAKE_CXX_COMPILER
-DCMAKE_RC_COMPILER
```

### Static analysis

You'll need include-what-you-use, but you can also just disable it be removing the line with DCMAKE_CXX_INCLUDE_WHAT_YOU_USE.
Note that not all warnings are significant and only you can decide if it make sens or not.
```
$ env CC=clang CXX=clang++ # CXX=clazy
$ cmake -DCMAKE_CXX_CLANG_TIDY:STRING='clang-tidy;--header-filter=APP/;-fix;-checks=-*,bugprone-*,cert-*,clang-analyzer-*,concurrency-*,cppcoreguidelines-*,google-*,modernize-*,performance-*,portability-*,readability-*,-readability-redundant-member-init,-cppcoreguidelines-avoid-magic-numbers,-readability-magic-numbers,-modernize-use-trailing-return-type,-cppcoreguidelines-pro-bounds-*,-modernize-use-auto,-cppcoreguidelines-avoid-c-arrays,-modernize-avoid-c-arrays,-modernize-pass-by-value,-cppcoreguidelines-pro-type-static-cast-downcast,-cppcoreguidelines-pro-type-member-init,-cppcoreguidelines-init-variables,-cppcoreguidelines-pro-type-reinterpret-cast,-cppcoreguidelines-pro-type-vararg,-cppcoreguidelines-pro-type-union-access,-cppcoreguidelines-macro-usage,-google-runtime-references,-google-runtime-int,-cppcoreguidelines-no-malloc,-cppcoreguidelines-owning-memory,-google-runtime-references,-google-readability-function-size,-readability-function-size,-cppcoreguidelines-avoid-goto,-cppcoreguidelines-avoid-non-const-global-variables,-readability-redundant-access-specifiers,-bugprone-macro-parentheses,-modernize-use-default-member-init,-cppcoreguidelines-non-private-member-variables-in-classes,-cppcoreguidelines-special-member-functions,-bugprone-forwarding-reference-overload' \
        -DCMAKE_CXX_INCLUDE_WHAT_YOU_USE='include-what-you-use;-Xiwyu;--mapping_file=/usr/local/share/include-what-you-use/iwyu.gcc.imp' \
        ..
$ ninja -k 0 -j 1 2> iwy.out
$ python fix_includes.py < iwy.out
```

## Run the tests

For both Windows and Debian :
The tests shall be run using the working directory containing the cmake build files (Release/Debug..)
```
$ ninja test
# Or this command: (use that in pipeline)
$ ctest --output-on-failure --schedule-random -j 4 # With 4 being you machine's CPU thread count
# On MSVC you may want to run (replacing release by your build type):
$ ctest --output-on-failure --schedule-random -j 4 -C Release
```

## Test coverage

To check test coverage you will need to compile in Debug mode using a GCC compiler (not clang or msvc) and then run the tests. When done, in your build folder you can run:
```
$ ninja coverage
```
You'll find the coverage data as html in the coverage folder in <yourbuildfolder>.

## Install

After going through the Build section you may want to install, run:
/!\ This'll install on your system! Also, you may want to install only the Release build.
```
$ sudo ninja install
```
