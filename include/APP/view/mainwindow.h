#ifndef APP_VIEW_MAINWINDOW_H
#define APP_VIEW_MAINWINDOW_H

#include <QMainWindow>
#include <memory>

QT_BEGIN_NAMESPACE
namespace Ui {
    class MainWindow;
}
QT_END_NAMESPACE

namespace app {

    class MainWindow : public QMainWindow {
    private:
        Q_OBJECT; // We need that so moc can help us connect slots correctly

    public:
    protected:
        using base_type = QMainWindow;

        static constexpr int kDefaultPointSize = 11;

    public:
        /// Hi
        ///
        MainWindow();

        /// Due to the pimpl we need to explicitly define the destuctory in the
        /// cc implementation
        ///
        virtual ~MainWindow();

    public Q_SLOTS:
        // format
        void DoActionToggleWordWrap();
        void DoActionStartFontWindow();

        // View
        void DoActionZoomIn();
        void DoActionZoomOut();
        void DoActionRestoreDefaultZoom();
        void DoActionToggleStatusBar();

        // Help
        void DoActionViewHelp();
        void DoActionSendFeedback();
        void DoActionAboutNotepad();

        // Status bar
        void DoActionUpdateCursorPos();
        void DoActionUpdateZoomLevel();
        void DoActionUpdateEOL();
        void DoActionUpdateEncoding();

    protected:
        std::unique_ptr<Ui::MainWindow> the_main_window_setup_;
    };

} // namespace app

#endif
