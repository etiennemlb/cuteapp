#ifndef APP_VIEW_APP_H
#define APP_VIEW_APP_H

#include <QApplication>

#include "APP/view/mainwindow.h"

namespace app {

    class App : public QApplication {
    public:
    protected:
        using base_type = QApplication;

    public:
        /// Hi
        ///
        App(int argc, char *argv[]);

    protected:
        MainWindow the_main_window_;
    };

} // namespace app

#endif
