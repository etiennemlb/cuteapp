#include <APP/view/app.h>

int main(int argc, char *argv[]) {
    return app::App{argc, argv}.exec();
}
