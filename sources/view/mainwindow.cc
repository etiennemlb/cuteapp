#include "APP/view/mainwindow.h"

#include <QDesktopServices>
#include <QDir>
#include <QMessageBox>

#include "APP/internal/externalconfig.h"
#include "ui_mainwindow.h"

namespace app {

    MainWindow::MainWindow()
        : the_main_window_setup_{std::make_unique<Ui::MainWindow>()} {

        the_main_window_setup_->setupUi(this);

        // Text box
        {
            connect(the_main_window_setup_->plainTextEdit, SIGNAL(cursorPositionChanged()), this, SLOT(DoActionUpdateCursorPos()));
        }

        // Format
        {
            connect(the_main_window_setup_->actionWord_Wrap, SIGNAL(triggered()), this, SLOT(DoActionToggleWordWrap()));
            connect(the_main_window_setup_->actionFont, SIGNAL(triggered()), this, SLOT(DoActionStartFontWindow()));
        }

        // View
        {
            connect(the_main_window_setup_->actionZoom_In, SIGNAL(triggered()), this, SLOT(DoActionZoomIn()));
            connect(the_main_window_setup_->actionZoom_Out, SIGNAL(triggered()), this, SLOT(DoActionZoomOut()));
            connect(the_main_window_setup_->actionRestore_Default_Zoom, SIGNAL(triggered()), this, SLOT(DoActionRestoreDefaultZoom()));
            connect(the_main_window_setup_->actionStatus_Bar, SIGNAL(triggered()), this, SLOT(DoActionToggleStatusBar()));
        }

        // Help
        {
            connect(the_main_window_setup_->actionView_Help, SIGNAL(triggered()), this, SLOT(DoActionViewHelp()));
            connect(the_main_window_setup_->actionSend_Feedback, SIGNAL(triggered()), this, SLOT(DoActionSendFeedback()));
            connect(the_main_window_setup_->actionAbout_Notepad, SIGNAL(triggered()), this, SLOT(DoActionAboutNotepad()));
        }

        // Status bar
        {
            the_main_window_setup_->statusbar->addPermanentWidget(the_main_window_setup_->cursor_pos);
            the_main_window_setup_->statusbar->addPermanentWidget(the_main_window_setup_->zoom_state);
            the_main_window_setup_->statusbar->addPermanentWidget(the_main_window_setup_->eol_state);
            the_main_window_setup_->statusbar->addPermanentWidget(the_main_window_setup_->encoding);

            // TODO(Etienne M): Add separators, I think they show up on windows but not on my linux window system

            DoActionUpdateCursorPos();
            DoActionUpdateZoomLevel();
            DoActionUpdateEOL();
            DoActionUpdateEncoding();
        }
    }

    void MainWindow::DoActionToggleWordWrap() {
        // We actually use a line wrap

        const QPlainTextEdit::LineWrapMode the_linewrap_mode = the_main_window_setup_->plainTextEdit->lineWrapMode();

        if(the_linewrap_mode == QPlainTextEdit::LineWrapMode::WidgetWidth) {
            the_main_window_setup_->plainTextEdit->setLineWrapMode(QPlainTextEdit::LineWrapMode::NoWrap);
        } else {
            the_main_window_setup_->plainTextEdit->setLineWrapMode(QPlainTextEdit::LineWrapMode::WidgetWidth);
        }
    }

    void MainWindow::DoActionStartFontWindow() {
    }


    void MainWindow::DoActionUpdateCursorPos() {
        const QTextCursor the_current_cursor_pos{the_main_window_setup_->plainTextEdit->textCursor()};

        the_main_window_setup_->cursor_pos->setText("Ln " +
                                                    QString::number(1) + // TODO(Etienne M)
                                                    ", Col " +
                                                    QString::number(1 + the_current_cursor_pos.columnNumber())); // Notepad start counting at 1
    }

    void MainWindow::DoActionUpdateZoomLevel() {
        const QFont the_current_font{the_main_window_setup_->plainTextEdit->font()};
        const int   the_current_point_size = the_current_font.pointSize();
        the_main_window_setup_->zoom_state->setText(QString::number(static_cast<unsigned>(100.0 * static_cast<double>(the_current_point_size) / static_cast<double>(kDefaultPointSize))) + " %");
    }

    void MainWindow::DoActionUpdateEOL() {
        if(the_main_window_setup_->plainTextEdit->toPlainText().isEmpty()) {
            // Maybe there is a way o get the size without copying everything..
            the_main_window_setup_->eol_state->setText("Windows (CRLF)");
        } else {
            // TODO(Etienne M): Fancy stuff
            the_main_window_setup_->eol_state->setText("Windows (CRLF)");
        }
    }

    void MainWindow::DoActionUpdateEncoding() {
        if(the_main_window_setup_->plainTextEdit->toPlainText().isEmpty()) {
            // Maybe there is a way o get the size without copying everything..
            the_main_window_setup_->encoding->setText("UTF-8");
        } else {
            // TODO(Etienne M): Fancy stuff only when loading a new file

            the_main_window_setup_->encoding->setText("UTF-8");
        }
    }


    void MainWindow::DoActionZoomIn() {
        the_main_window_setup_->plainTextEdit->zoomIn();
        DoActionUpdateZoomLevel();
    }

    void MainWindow::DoActionZoomOut() {
        the_main_window_setup_->plainTextEdit->zoomOut();
        DoActionUpdateZoomLevel();
    }

    void MainWindow::DoActionRestoreDefaultZoom() {
        QFont the_current_font{the_main_window_setup_->plainTextEdit->font()};
        the_current_font.setPointSize(kDefaultPointSize);
        the_main_window_setup_->plainTextEdit->setFont(the_current_font);
        DoActionUpdateZoomLevel();
    }

    void MainWindow::DoActionToggleStatusBar() {
        the_main_window_setup_->statusbar->setVisible(!the_main_window_setup_->statusbar->isVisible());
    }


    void MainWindow::DoActionViewHelp() {
        QDesktopServices::openUrl(QUrl{"https://www.bing.com/search?q=get+help+with+notepad+in+windows+10"});
    }

    void MainWindow::DoActionSendFeedback() {
        QMessageBox::warning(this, "Not implemented", "This feature is not yet implemented.");
    }

    void MainWindow::DoActionAboutNotepad() {
        QMessageBox::about(this, "About Notepad",
                           "Etienne Malaboeuf\n"
                           "Version " APP_VERSION
                           "\n"
                           "© 2021 Etienne Malaboeuf Corporation. All rights reserved.\n\n"
                           "The XXX operation systems XXXX\n\n"
                           "This product is lisenced under the Etienne Malaboeuf Software License\n"
                           "Terms to:\n"
                           "    " +
                               QDir::home().dirName());
        // QMessageBox::aboutQt(this);
    }

    MainWindow::~MainWindow() {
        // EMPTY
    }

} // namespace app
