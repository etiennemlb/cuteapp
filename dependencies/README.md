# Adding a dependency
## Rules

- `/dependencies` should only contain folder and this `README.md` file.
- A dependency should be a git submodule (to ease the version control of the dependencies) containing a CMakeLists.txt
file or at least a directory containing a CMakeLists.txt.

## Methods

Always create a file called `lib.cmake` in a folder named after the dependency imported.

- By updating a git submodule:
```
$ git submodule add ../../owner/repo.git dependencies/<the_repo_folder_name>
```
the_repo_folder_name shall contain a CMakeLists.txt file.

- By downloading at configure time:
```
# include(FetchContent) # Already included in main CMakeLists
FetchContent_Declare(embree
                     GIT_REPOSITORY https://github.com/embree/embree.git
                     GIT_TAG        v3.13.1)

# Expects to find a CMakeLists.txt in the downloaded repo.
FetchContent_MakeAvailable(embree)
```
