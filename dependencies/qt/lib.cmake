# use
# target_link_libraries(<<my_target>>
#                        PRIVATE Qt5::Widgets)
# to link with Qt5::Widgets

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

find_package(Qt5 COMPONENTS Widgets REQUIRED)
